# Initiation au CSS
Amusons nous avec les DIV's !
Hier, vous avez découvert le HTML et le CSS ? Il est temps de faire votre première maquette ! Dans le développement web, une maquette est l'image qu'un client vous donne et que vous devez reproduire en intégration.

Pour cet exercice il vous faudra donc transcrire en HTML et en CSS l'image ci-dessous.

Avant de vous lancer, prenez le temps de regarder l'image concernée. Ce n'est pas grave si les couleurs et les images ne sont pas identiques à celles de la maquette, ni si vous ne mettez pas les textes en forme de la même façon, ce qui compte pour cet exercice c'est l'approche que vous aurez face au problème. Pour les images, vous pouvez utiliser des icônes provenant de The Noun Project ou encore des images venant du site Placehold.it voire des icones de flat icon.

Pour vous aider, voici quelques tips :

* Prenez une feuille papier crayons pour définir vos divs. Il faudra des divs permettant uniquement de contenir d'autres divs.
* Certains blocs semblent se répeter. Définissez vos classes CSS.
* La zone des tuiles en dessous est composée de trois colonnes
* la première colonne est composée de 3 blocs, le premier prenant toute la largeur et les 2 autres la moitié
* On ne vous détaille pas la logique pour la 2ème et la 3ème colonne, le principe est le même.

Bon courage et bon code ! :)


![DIVs](./img/exoDivs.jpg) 

<style>
    a {
        color: white;
    }
    </style>
<a href="sdfghjkl"> je suis un lien</a>

